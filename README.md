# Summary

1. [Languages and their respectives Web Frameworks](#languages-and-their-respectives-web-frameworks)
    * [Languages: Interpreted, dynamically typed](#languages-interpreted-dynamically-typed)
        * [ELIXIR](#elixir)
        * [RUBY](#ruby)
        * [PYTHON](#python)
        * [PHP](#php)
    * [Languages: Natively compiled, statically typed](#languages-natively-compiled-statically-typed)
        * [CRYSTAL](#crystal)
        * [RUST LANG](#rust-lang)        
        * [GO LANG](#go-lang)

# Languages and their respectives Web Frameworks

## Languages: Interpreted, dynamically typed

### [Elixir](https://elixir-lang.org/)

- [Phoenix](http://phoenixframework.org) - https://github.com/phoenixframework/phoenix

> Productive. Reliable. Fast. A productive web framework that does not compromise speed and maintainability.

### [Ruby](https://www.ruby-lang.org/)

- [Ruby on Rails](http://rubyonrails.org/) - https://github.com/rails/rails

> Ruby on Rails makes it much easier and more fun. It includes everything you need to build fantastic applications, and you can learn it with the support of our large, friendly community.

- [Hanami](http://hanamirb.org/) - https://github.com/hanami/hanami

> The web, with simplicity
 
### [Python](https://www.python.org/)

- [CherryPy](https://cherrypy.org/) - https://github.com/cherrypy/cherrypy

> CherryPy is a pythonic, object-oriented web framework: CherryPy allows developers to build web applications in much the same way they would build any other object-oriented Python program. This results in smaller source code developed in less time. CherryPy is now more than ten years old and it is has proven to be very fast and stable. It is being used in production by many sites, from the simplest to the most demanding.

- [Falcon](http://falconframework.org/) - https://github.com/falconry/falcon

> Falcon is a bare-metal Python web API framework for building very fast app backends and microservices.

- [Flask](http://flask.pocoo.org/) - https://github.com/pallets/flask

> Flask is a microframework for Python based on Werkzeug, Jinja 2 and good intentions. And before you ask: It's BSD licensed! 

- [Django](https://www.djangoproject.com/) - https://github.com/django/django

> Django makes it easier to build better Web apps more quickly and with less code.

- [Pyramid](https://trypyramid.com/) - https://github.com/Pylons/pyramid

> The Start Small, Finish Big Stay Finished Framework: Projects with ambition start small but finish big and must stay finished. You need a Python web framework that supports your decisions, 
by artisans for artisans.

### [PHP](http://www.php.net/)

- [Laravel](https://laravel.com/) - https://github.com/laravel/laravel

> Love beautiful code? We do too. The PHP Framework For Web Artisans

## Languages: Natively compiled, statically typed

### [Crystal](https://crystal-lang.org/)

Crystal's trendings on Github: https://github.com/trending/crystal

- [Amber](https://amberframework.org/) - https://github.com/amberframework/amber

> A Crystal web framework that makes building applications fast, simple, and enjoyable. Get started with quick prototyping, less bugs, and blazing fast performance.

- [Kemal](http://kemalcr.com/) - https://github.com/kemalcr/kemal

> Lightning Fast, Super Simple web framework.

- [Lucky](https://luckyframework.org/) - https://github.com/luckyframework/lucky
 
>  beautiful new web framework written in Crystal. Catch bugs early, forget about most performance issues, and spend more time on code instead of debugging and writing tests. 

- Amethyst - https://github.com/amethyst-framework/amethyst

> Amethyst is a Rails inspired web-framework for Crystal language
>>  :warning: Amethyst is currently undergoing a re-write from the ground up.

### [Rust-lang](https://www.rust-lang.org/)

- [Rocket](https://rocket.rs/) - https://github.com/SergioBenitez/Rocket

> Rocket is web framework for Rust (nightly) with a focus on ease-of-use, expressibility, and speed. Here's an example of a complete Rocket application:

- [Hyper](https://hyper.rs/) - https://github.com/hyperium/hyper

> Fast and safe HTTP for the Rust language.

- [Nickel](http://nickel-org.github.io/) - https://github.com/nickel-org/nickel.rs

> Nickel.rs is a simple and lightweight foundation for web applications written in Rust. Its API is inspired by the popular express framework for JavaScript.

### [Go-lang](https://golang.org/)

- [Gin](https://gin-gonic.github.io/gin) - https://github.com/gin-gonic/gin

> HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance.

- [Beego](https://beego.me) - https://github.com/astaxie/beego

> An open-source, high-performance web framework for the Go programming language.

- [Echo](https://echo.labstack.com) - https://github.com/labstack/echo

> A high performance, minimalist Go web framework.

- [Revel](https://revel.github.io) - https://github.com/revel/revel

> Revel: A high productivity, full-stack web framework for the Go language.

- [Iris](https://iris-go.com) - https://github.com/kataras/iris

> The fastest web framework for Go in The Universe. MVC fully featured. Embrace the future today.
    
- [Buffalo](https://gobuffalo.io) - https://github.com/gobuffalo/buffalo

> Rapid Web Development w/ Go.